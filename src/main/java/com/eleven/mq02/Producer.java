package com.eleven.mq02;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class Producer {

    public static void main(String[] args) {
        try {
            sendByExchange("hello");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendByExchange(String message) throws Exception {
        Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明队列
        // 第二个参数表示队列是否持久化（跟消息持久化不同），
        // 第三个参数表示是否是排他队列，表示这个队列只能绑定在这个链接上。
        // 第四个参数，表示是否队列为空时删除，一般结合第三个参数为ture使用true
        // 第五个参数为队列配置参数集合
        channel.queueDeclare(ConnectionUtil.QUEUE_NAME, true, false, false, null);
        // 声明exchange （交换机名称  交换机类型）
        channel.exchangeDeclare(ConnectionUtil.EXCHANGE_NAME, "fanout");
        //交换机和队列绑定
        channel.queueBind(ConnectionUtil.QUEUE_NAME, ConnectionUtil.EXCHANGE_NAME, "");
        //basicPublish参数：1交换机 2路由键 3AMQP基本参数 4消息内容字节数组
        channel.basicPublish(ConnectionUtil.EXCHANGE_NAME, "", null,
                message.getBytes());
        System.out.println("发送的信息为:" + message);
        channel.close();
        connection.close();
    }

}
