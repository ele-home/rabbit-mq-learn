package com.eleven;

import com.alibaba.fastjson.JSONObject;
import com.eleven.common.HttpTools;

import java.util.Random;

public class Test4paradigm {

    public static void main(String[] args) {
        // requestID:请求id，随机的字符串
        // sceneID:场景ID
        // userId:用户ID，能标识用户的唯一值
        Long userId = 1002290953L;
//        String recommUrl = "http://localhost:8080/xj/getLastPage?userId=%s&num=50";
        String recommUrl = "http://localhost:8080/xj/getGuessYouLik?userId=%s";
//        JSONObject postJson = new JSONObject();
//        postJson.put("page", 0);
        int cycleNum = 1000;  //循环调用次数
        Long totalTime = 0L; //总耗时
        for (int i = 0; i < cycleNum; i++) {
            Long begTime;
            Long endTime;
            Long useTime;
//            String userId = "100" + i +90+ new Random().nextInt(1000);
            String url = String.format(recommUrl,userId);
            begTime= System.currentTimeMillis();
            String result = HttpTools.get(url, HttpTools.HTTP_TIMEOUT_MS);
//            System.out.println("返回结果为：" + result);
            endTime = System.currentTimeMillis();
            useTime = endTime - begTime;
            System.out.println(i+"  "+ userId +"  "+ "useTime = "+useTime);
            totalTime = totalTime + useTime;
        }
        System.out.println("总耗时 totalTime = " + totalTime);
        double tempNum = cycleNum * 1000;
        System.out.println("平均耗时 averageTime = " + totalTime/tempNum +" 秒");
        System.out.println("平均耗时 averageTime = " + totalTime/cycleNum +" 毫秒");
    }

}
