rabbitmq自带管理后台，安装后需要配置开启

进入rabbitmq安装目录中的sbin目录执行

rabbitmq-plugins enable rabbitmq_management

重启rabbitmq服务生效

管理后台地址
http://localhost:15672/


用户名密码均为
guest
guest

-- 本地MQ安装地址

D:\Program Files\RabbitMQ Server\rabbitmq_server-3.8.0\sbin

-- 添加可视化插件
-- 启动
rabbitmq-plugins enable rabbitmq_management
-- 关闭
rabbitmq-plugins disable rabbitmq_management

-- 
rabbitmq-server -detached 后台启动

Rabbitmq-server    直接启动，如果你关闭窗口或者需要在改窗口使用其他命令时应用就会停止

rabbitmqctl stop   关闭

rabbitmqctl status  查看状态


=============================================================== RabbitMQ 清除全部队列及消息

前言

安装RabbitMQ后可访问：http://{rabbitmq安装IP}:15672使用(默认的是帐号guest，密码guest。此账号只能在安装RabbitMQ的机器上登录，无法远程访问登录。）
远程访问登录，可以使用自己创建的帐号，给与对应的管理员权限即可。
直接在管理页面删除

访问http://{rabbitmq安装IP}:15672，登录。
点击queues，这里可以看到你创建的所有的Queue，
选中某一个Queue，下方有个Delete Queue删除队列/Purge Message清除消息。
但是这样只能一个队列一个队列的删除，如果队列中的消息过多就会特别慢。
命令行批量删除

首先定位到 rabbitMQ 安装目录的sbin 目录下。打开cmd窗口。
关闭应用的命令为： rabbitmqctl stop_app
清除的命令为： rabbitmqctl reset
重新启动命令为： rabbitmqctl start_app
ps
查看所有队列命令： rabbitmqctl list_queues
注意

命令行批量删除相当于重置RabbitMQ，请谨慎操作，配置和队列都会被清除。
操作完成，记得添加远程访问账号。


===============================================================



文章看一下
https://blog.csdn.net/vbirdbest/article/details/78699913

https://blog.csdn.net/li1987by/article/details/90314101

https://www.cnblogs.com/lfalex0831/p/8963247.html


RabbitMQ教程
https://blog.csdn.net/hellozpc/article/details/81436980


